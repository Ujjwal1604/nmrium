export interface NMRiumToolBarPreferences {
  zoomTool: boolean;
  zoomOutTool: boolean;
  import: boolean;
  exportAs: boolean;
  spectraStackAlignments: boolean;
  spectraCenterAlignments: boolean;
  realImaginary: boolean;
  peakTool: boolean;
  integralTool: boolean;
  zonePickingTool: boolean;
  slicingTool: boolean;
  rangePickingTool: boolean;
  zeroFillingTool: boolean;
  apodizationTool: boolean;
  phaseCorrectionTool: boolean;
  baselineCorrectionTool: boolean;
  FFTTool: boolean;
  multipleSpectraAnalysisTool: boolean;
  exclusionZonesTool: boolean;
}
